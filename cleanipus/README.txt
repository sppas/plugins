------------------------------------------------------------------------------

program:    cleantrsipus
author:     Brigitte Bigi
contact:    develop@sppas.org
date:       2019-02-16, 02-01-2021, 2023-08-19
version:    1.2
copyright:  Copyright (C) 2019-2023 Brigitte Bigi
license:    GNU Public License version 3 or any later version
brief:      SPPAS plugin to create clean transcribed IPUs.

This plugin allows to clean the IPUs after the orthographic transcription 
was performed: it removes the un-transcribed IPUs, re-indexes, it merges 
consecutive silences, etc.

------------------------------------------------------------------------------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

------------------------------------------------------------------------------
