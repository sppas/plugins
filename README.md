    -------------------------------------------------------------------------

                              P L U G I N S

    -------------------------------------------------------------------------

    This is the set of plugins for SPPAS.

    -------------------------------------------------------------------------

    SPPAS - the automatic annotation and analyses of speech, is a "Research
    Software". It is a scientific computer software package written and
    maintained by Brigitte Bigi, researcher in Computer Science at the
    Laboratoire Parole et Langage, in Aix-en-Provence, France.

    * SPPAS main page: <https://sppas.org/>
    * SPPAS package: <https://sourceforge.net/projects/sppas>
    * Author home page: https://sppas.org/bigi/
    * Author ORCID: https://orcid.org/0000-0003-1834-6918

    -------------------------------------------------------------------------

    * Any and all constructive comments are welcome: `contact@sppas.org`
    * Declare an issue or propose a plugin: `develop@sppas.org`

    -------------------------------------------------------------------------
